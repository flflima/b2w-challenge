select (
	select value from events where event_type = 2 and time = (select time from events where event_type = 2 order by time desc LIMIT 1, 1)) - 
	(select value from events where event_type = 2 and time = (select min(time) from events e where e.event_type = 2)) as value, 
	(select event_type from events where event_type = 2 and time = (select max(time) from events e where e.event_type = 2)) as event_type 
order by event_type;
