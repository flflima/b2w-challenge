create table events (
event_type integer not null,
value integer not null,
time timestamp not null,
unique (event_type, time)
);
