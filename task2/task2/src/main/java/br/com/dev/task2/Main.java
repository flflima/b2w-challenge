package br.com.dev.task2;

public class Main {

    public static int fatorial(final int val) {
	if (val <= 0)
	    return 1;
	return val * fatorial(val - 1);
    }

    public static void main(String[] args) {
	if (args.length <= 0) {
	    throw new RuntimeException("Faltando parâmetro de entrada.");
	} else {
	    final String numero = args[0];
	    System.out.println("Valor = " + numero + "; Tamanho " + numero.length());

	    // calcular o número de combinações
	    int numCombinacoes = fatorial(numero.length());

//	    System.out.println("Total de combinações: " + numCombinacoes);

	    int numeroInt = Integer.parseInt(numero);

	    int divFactor = 1;

	    for (int i = 1; i < numero.length(); i++) {
		divFactor *= 10;
	    }

//	    System.out.println("Div Factor = " + divFactor);

	    int totalZeros = 0;
	    int totalNonZeros = 0;

	    while (divFactor > 0) {
		int dig = numeroInt / divFactor;
		numeroInt %= divFactor;
		divFactor /= 10;

		if (dig == 0) {
		    totalZeros++;
		} else {
		    totalNonZeros++;
		}
	    }
	    
	    numCombinacoes = numCombinacoes - (totalNonZeros * totalZeros);

	    System.out.println("Output = " + numCombinacoes);
	}
    }
}
